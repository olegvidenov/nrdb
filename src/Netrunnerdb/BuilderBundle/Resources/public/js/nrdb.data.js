if (typeof NRDB != "object")
	var NRDB = { 
		data_loaded: jQuery.Callbacks(), 
		api_url: {
			sets: 'http://netrunnerdb.com/api/sets/',
			cards: 'http://netrunnerdb.com/api/cards/',
			mwl: 'http://netrunnerdb.com/api/get_mwl/'
		},
		locale: 'en'
	};
NRDB.data = {};
(function(data, $) {
	data.sets = {};
	data.cards = {};
	data.mwl = {};

	var sets_data = null;
	var cards_data = null;
	var mwl_data = null;
	var is_modified = null;

	data.query = function() {
		data.initialize();
		data.promise_sets = $
				.ajax(NRDB.api_url.sets+"?jsonp=NRDB.data.parse_sets&_locale="
						+ NRDB.locale);
		data.promise_cards = $
				.ajax(NRDB.api_url.cards+"?jsonp=NRDB.data.parse_cards&_locale="
						+ NRDB.locale);
		data.promise_mwl = $
				.ajax(NRDB.api_url.mwl+"?jsonp=NRDB.data.parse_mwl");

		$.when(data.promise_sets, data.promise_cards, data.promise_mwl).done(data.initialize);
	};

	data.initialize = function() {
		if (is_modified === false)
			return;

		var json;

		try {
			sets_data = sets_data || (json = localStorage.getItem('sets_data_' + NRDB.locale) && JSON.parse(json));
		} catch(e) {
			sets_data = [];
		}
		if(!sets_data) return;
		data.sets = TAFFY(sets_data);
		data.sets.sort("cyclenumber,number");

		try {
			cards_data = cards_data || (json = localStorage.getItem('cards_data_' + NRDB.locale) && JSON.parse(json));
		} catch(e) {
			cards_data = [];
		}
		if(!cards_data) return;
		data.cards = TAFFY(cards_data);
		data.cards.sort("code");

		try {
			mwl_data = mwl_data || (json = localStorage.getItem('mwl_data') && JSON.parse(json));
		} catch(e) {
			mwl_data = [];
		}
		if(!mwl_data) return;
		data.mwl = TAFFY(mwl_data);
		data.mwl.sort("start");

		NRDB.data_loaded.fire();
	};

	data.parse_sets = function(response) {
		if(typeof response === "undefined") return;
		var json = JSON.stringify(sets_data = response);
		is_modified = is_modified
				|| json != localStorage.getItem("sets_data_" + NRDB.locale);
		localStorage.setItem("sets_data_" + NRDB.locale, json);
	};

	data.parse_cards = function(response) {
		if(typeof response === "undefined") return;
		var json = JSON.stringify(cards_data = response);
		is_modified = is_modified
				|| json != localStorage.getItem("cards_data_" + NRDB.locale);
		localStorage.setItem("cards_data_" + NRDB.locale, json);
	};

	data.parse_mwl = function(response) {
		if(typeof response === "undefined") return;
		mwl_data = response.version ? response.data : response;
		var json = JSON.stringify(mwl_data);
		is_modified = is_modified
			|| json != localStorage.getItem("mwl_data");
		localStorage.setItem("mwl_data", json);
	};
	
	data.get_card_by_code = function(code) {
		if(data.cards) {
			return data.get_cards_by_code(code).first();
		}
	};
	
	data.get_cards_by_code = function(code) {
		if(data.cards) {
			return data.cards({code:String(code)});
		}
	};
	
	$(function() {
		if(NRDB.api_url) data.query();
	});

})(NRDB.data, jQuery);



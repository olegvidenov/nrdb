<?php


namespace Netrunnerdb\BuilderBundle\Services;


/*
 *
 */
class Judge
{
	public function __construct($doctrine) {
		$this->doctrine = $doctrine;
	}
	
	/**
	 * Decoupe un deckcontent pour son affichage par type
	 *
	 * @param \Netrunnerdb\CardsBundle\Entity\Card $identity
	 */
	public function classe($cards, $identity)
	{
		$analyse = $this->analyse($cards);
		
		$classeur = array();
		/* @var $slot \Netrunnerdb\BuilderBundle\Entity\Deckslot */
		foreach($cards as $elt) {
			/* @var $card \Netrunnerdb\CardsBundle\Entity\Card */
			$card = $elt['card'];
			$qty = $elt['qty'];
			$type = $card->getType()->getName();
			if($type == "Identity") continue;
			if($type == "ICE") {
				$keywords = explode(" - ", $card->getKeywords());
				if(in_array("Barrier", $keywords)) $type = "Barrier";
				if(in_array("Code Gate", $keywords)) $type = "Code Gate";
				if(in_array("Sentry", $keywords)) $type = "Sentry";
			}
			if($type == "Program") {
				$keywords = explode(" - ", $card->getKeywords());
				if(in_array("Icebreaker", $keywords)) $type = "Icebreaker";
			}
			$influence = 0;
			$qty_influence = $qty;
			if($identity->getCode() == "03029" && $card->getType()->getName() == "Program") $qty_influence--;
			if($card->getFaction()->getId() != $identity->getFaction()->getId()) $influence = $card->getFactionCost() * $qty_influence;
			if($card->getCode() === '10018') {
				// Mumba Temple: 15 or fewer ice => 0 inf
				$targets = array_filter($cards, function ($potentialTarget) {
					return $potentialTarget['card']->getType()->getName() === 'ICE';
				});
				$count = array_reduce($targets, function ($carry, $item) { return $carry + $item['qty']; }, 0);
				if($count <= 15) {
					$influence = 0;
				}
			}
			if(in_array($card->getCode(), array('10068', '10067', '10071', '10072'))) {
				// 6 or more non-alliance cards of the same faction
				$targets = array_filter($cards, function ($potentialTarget) {
					return $potentialTarget['card']->getFaction()->getId() === $card->getFaction()->getId()
						&& strpos('Alliance', $potentialTarget['card']->getKeywords()) === -1;
				});
				$count = array_reduce($targets, function ($carry, $item) { return $carry + $item['qty']; }, 0);
				if($count >= 6) {
					$influence = 0;
				}
			}
			$elt['influence'] = $influence;
			$elt['faction'] = str_replace(' ', '-', mb_strtolower($card->getFaction()->getName()));
			
			if(!isset($classeur[$type])) $classeur[$type] = array("qty" => 0, "slots" => array());
			$classeur[$type]["slots"][] = $elt;
			$classeur[$type]["qty"] += $qty;
		}
		if(is_string($analyse)) {
			$classeur['problem'] = $this->problem($analyse);
		} else {
			$classeur = array_merge($classeur, $analyse);
		}
		return $classeur;
	}
	
    /**
     * Analyse un deckcontent et renvoie un code indiquant le pbl du deck
     *
     * @param array $content
     * @return array
     */
	public function analyse($cards)
	{
		$identity = null;
		$deck = array();
		$deckSize = 0;
		$influenceSpent = 0;
		$agendaPoints = 0;
		
		foreach($cards as $elt) {
			$card = $elt['card'];
			$qty = $elt['qty'];
			if($card->getType()->getName() == "Identity") {
			    if(isset($identity)) return 'identities';
				$identity = $card;
			} else {
				$deck[] = $card;
				$deckSize += $qty;
			}
		}
		
		if(!isset($identity)) {
			return 'identity';
		}
		
		if($deckSize < $identity->getMinimumDeckSize()) {
			return 'deckSize';
		}
		
		foreach($deck as $card) {
			$qty = $cards[$card->getCode()]['qty'];
			
			if($qty > $card->getLimited() && $identity->getPack()->getCode() != "draft") {
			    return 'copies';
			}
			if($card->getSide() != $identity->getSide()) {
				return 'side';
			}
			if($identity->getCode() == "03002" && $card->getFaction()->getName() == "Jinteki") {
				return 'forbidden';
			}
			if($card->getType()->getName() == "Agenda") {
				if($card->getFaction()->getName() != "Neutral" && $card->getFaction() != $identity->getFaction() && $identity->getFaction()->getName() != "Neutral") {
					return 'agendas';
				}
				$agendaPoints += $card->getAgendaPoints() * $qty;
			}
			if($card->getFaction() != $identity->getFaction()) {
				if($identity->getCode() == "03029" && $card->getType()->getName() == "Program") {
					$influenceSpent += $card->getFactionCost() * ($qty - 1);
				} else {
					$influenceSpent += $card->getFactionCost() * $qty;
				}
			}
		}
		
		if($identity->getInfluenceLimit() !== null && $influenceSpent > $identity->getInfluenceLimit()) return 'influence';
		// agenda points rule, except for neutral identities because they are used for Cube lists
		if($identity->getSide()->getName() == "Corp" && $identity->getPack()->getName() != "Neutral") {
			$minAgendaPoints = floor($deckSize / 5) * 2 + 2;
			if($agendaPoints < $minAgendaPoints || $agendaPoints > $minAgendaPoints + 1) return 'agendapoints';
		}
		
		return array(
			'deckSize' => $deckSize,
			'influenceSpent' => $influenceSpent,
			'agendaPoints' => $agendaPoints
		);
	}
	
	public function problem($problem)
	{
		switch($problem) {
			case 'identity': return "The deck lacks an Identity card."; break;
			case 'identities': return "The deck has more than 1 Identity card;"; break;
			case 'deckSize': return "The deck has less cards than the minimum required by the Identity."; break;
			case 'side': return "The deck mixes Corp and Runner cards."; break;
			case 'forbidden': return "The deck includes forbidden cards."; break;
			case 'agendas': return "The deck uses Agendas from a different faction."; break;
			case 'influence': return "The deck spends more influence than available on the Identity."; break;
			case 'agendapoints': return "The deck has a wrong number of Agenda Points."; break;
			case 'copies' : return "The deck has too many copies of a card."; break;
		}
	}
	
}